from numba import cuda
import numba
import math, time
import numpy as np
import cupy as cp

N = 2**27
THREADSPERBLOCK = 1024
NBLOCKS = N // THREADSPERBLOCK


@cuda.jit(device=True)
def Scan(a):
    threadid = cuda.threadIdx.x
    blockid = cuda.blockIdx.x
    globalindex = blockid * THREADSPERBLOCK + threadid

    value = a[threadid]

    steps = int(math.log(float(THREADSPERBLOCK)) / math.log(2.0)) - 1

    # Upsweep
    for d in range(steps):
        if threadid % 2**(d + 1) == 0:
            idx1 = threadid + 2**(d + 1) - 1
            idx2 = threadid + 2**(d) - 1
            a[idx1] += a[idx2]
        cuda.syncthreads()

    a[THREADSPERBLOCK - 1] = 0

    # Downsweep
    for d in range(steps, -1, -1):
        if threadid % 2**(d + 1) == 0:
            idx1 = int(threadid + 2**(d) - 1)
            idx2 = int(threadid + 2**(d + 1) - 1)
            t = a[idx1]
            a[idx1] = a[idx2]
            a[idx2] += t
        cuda.syncthreads()

    # Convert from exclusive to inclusive scan
    a[threadid] += value


@cuda.jit
def Split(a, bit, out):
    threadid = cuda.threadIdx.x
    blockid = cuda.blockIdx.x
    globalindex = blockid * THREADSPERBLOCK + threadid

    value = a[globalindex]
    bitvalue = (value & 2**bit) >> bit

    shared = cuda.shared.array(THREADSPERBLOCK, dtype=numba.int32)
    shared[threadid] = bitvalue

    cuda.syncthreads()
    Scan(shared)
    cuda.syncthreads()

    falsecount = cuda.shared.array(1, dtype=numba.int32)
    if threadid == 0:
        falsecount[0] = THREADSPERBLOCK - shared[-1]
    cuda.syncthreads()

    newindex = 0
    if bitvalue == 0:
        newindex = threadid - shared[threadid]
    else:
        newindex = falsecount[0] + shared[threadid] - 1
    out[blockid * THREADSPERBLOCK + newindex] = value


@cuda.jit
def ComputeHistogram(a, bit, histogram, bucketoffsets):
    threadid = cuda.threadIdx.x
    blockid = cuda.blockIdx.x
    globalindex = blockid * THREADSPERBLOCK + threadid
    bucketstart = blockid * 16

    if threadid < 16:
        bucketoffsets[bucketstart + threadid] = 0
        histogram[bucketstart + threadid] = 0

    cuda.syncthreads()

    radix = (a[globalindex] >> bit) & 0xF
    nextradix = 0
    if threadid < THREADSPERBLOCK - 1:
        nextradix = (a[globalindex + 1] >> bit) & 0xF
        if radix != nextradix:
            bucketoffsets[bucketstart + nextradix] = threadid + 1

    cuda.syncthreads()

    if threadid < THREADSPERBLOCK - 1:
        if radix != nextradix:
            count = bucketoffsets[bucketstart +
                                  nextradix] - bucketoffsets[bucketstart +
                                                             radix]
            histogram[bucketstart + radix] = count
    else:
        count = THREADSPERBLOCK - bucketoffsets[bucketstart + radix]
        histogram[bucketstart + radix] = count


@cuda.jit
def MakeColumnMajor(a, out):
    threadid = cuda.threadIdx.x
    blockid = cuda.blockIdx.x

    globalindex = blockid * 16 + threadid
    value = a[globalindex]

    index = NBLOCKS * threadid + blockid

    # Shift all elements one to the right.
    # This is done so cumsum computes an exclusive scan.
    if index < len(a) - 1:
        out[index + 1] = value
    else:
        out[0] = 0


@cuda.jit
def GlobalSort(a, bit, bucketoffsets, histogramscan, out):
    threadid = cuda.threadIdx.x
    blockid = cuda.blockIdx.x
    globalindex = blockid * THREADSPERBLOCK + threadid
    bucketstart = blockid * 16

    value = a[globalindex]

    radix = (value >> bit) & 0xF
    radixoffset = histogramscan[radix * NBLOCKS + blockid]

    out[radixoffset + threadid - bucketoffsets[bucketstart + radix]] = value


def RadixSort(A):
    start = time.time()

    B = cuda.device_array_like(A)
    histogram = cuda.device_array(16 * NBLOCKS, dtype=np.int32)
    histogramscan = cuda.device_array(16 * NBLOCKS, dtype=np.int32)
    bucketoffsets = cuda.device_array(16 * NBLOCKS, dtype=np.int32)

    for k in range(8):
        bit = 4 * k
        for i in range(4):
            Split[NBLOCKS, THREADSPERBLOCK](A, bit + i, B)
            A, B = B, A

        ComputeHistogram[NBLOCKS, THREADSPERBLOCK](A, bit, histogram,
                                                   bucketoffsets)

        MakeColumnMajor[NBLOCKS, 16](histogram, histogramscan)
        cphistogram = cp.asarray(histogramscan)
        cp.cumsum(cphistogram, out=cphistogram)

        GlobalSort[NBLOCKS, THREADSPERBLOCK](A, bit, bucketoffsets,
                                             histogramscan, B)
        A, B = B, A

    end = time.time()
    # Benchmark is captured inside the function to avoid including the time to free the device arrays.
    print(end - start)


A = (np.random.rand(N) * 2**32).astype(np.uint32)
sorted_A = np.sort(A)

A = cuda.to_device(A)
RadixSort(A)

print(np.array_equal(sorted_A, A))